#include <iostream>
#include <random>
#include <string.h>
#include <assert.h>

using namespace std;

void gen_new_pi_vector(int num_alpha, int num_beta);
void gen_old_pi_vector(int num_alpha, int num_beta);

void gen_new_std_vector(int num_alpha, int num_beta);
void gen_old_std_vector(int num_alpha, int num_beta);

std::default_random_engine generator;
std::uniform_int_distribution<int> alphadistribution(0, 99);
std::uniform_int_distribution<int> betadistribution(0, 1);

// ./generating_data_vectors --num 100 --old 0 --pi 40 --nSlots 100 --beta 20 --alpha 10
// ./generating_data_vectors --num 630 --pi 40 --nSlots 210 --beta 40 --alpha 20

int main(int argc, char* argv[])
{

  int num_vectors = 180;
  int num_alpha = 10;
  int num_beta = 20;

  int old_or_new = 0; //0 = new and 1 = old
  int pi = 30;
  int nSlots = 210;

  int arg = 0;
  while (arg < argc-1)
  {
    if( !strcmp(argv[arg],("--num")) || !strcmp(argv[arg],("-n")) )
    {
      num_vectors = stoi(argv[arg+1]);
      arg++;
    }
    if( !strcmp(argv[arg],("--alpha")) || !strcmp(argv[arg],("-a")) )
    {
      num_alpha = stoi(argv[arg+1]);
      arg++;
    }
    if( !strcmp(argv[arg],("--beta")) || !strcmp(argv[arg],("-b")) )
    {
      num_beta = stoi(argv[arg+1]);
      arg++;
    }
    if( !strcmp(argv[arg],("--pi")) || !strcmp(argv[arg],("-p")) )
    {
      pi = stoi(argv[arg+1]);
      arg++;
    }
    if( !strcmp(argv[arg],("--old")) || !strcmp(argv[arg],("-o")) )
    {
      old_or_new = stoi(argv[arg+1]);
      arg++;
    }
    if( !strcmp(argv[arg],("--nslots")) || !strcmp(argv[arg],("--nSlots")) || !strcmp(argv[arg],("-s")) )
    {
      nSlots = stoi(argv[arg+1]);
      arg++;
    }
    arg++;
  }

  assert(num_vectors % nSlots == 0);
  if ( old_or_new )
    assert(pi < nSlots);
  asser(pi < num_vectors);
  if (beta < alpha)
    cout << "Beta < Alpha.  Probably not right"

  if(old_or_new) /* old_or_new == 1 iff we want an old style data set */{
    // Generating the real data set
    for( int vec = 0; vec < num_vectors - pi; vec++ ) 
      gen_old_std_vector(num_alpha, num_beta);

    for( int vec = 0; vec < pi; vec++ ) 
      gen_old_pi_vector(num_alpha, num_beta);
  }
  else 
  {
    for( int vec = 0 ; vec < num_vectors; vec++)
    {
      if ( vec % nSlots == pi)
      {
        gen_new_pi_vector(num_alpha, num_beta); 
      }
      else 
      {
        gen_new_std_vector(num_alpha, num_beta);
      }
    }
  }
}

void gen_old_std_vector(int num_alpha, int num_beta) 
{
  cout << "{";

  for (int i = 0; i < num_alpha; i++) 
  {
    int output = alphadistribution(generator);
    if ( output < 10)
    {
      cout << "  " << output << ","; 
    } else 
    {
     cout << " " << output << ","; 
    }
  }

  // Make sure there are the approprite zeros
  for(int i = 0; i < num_alpha; i++)
  {
    cout << "  0,";
  }

  for (int i = 0; i < num_beta; i++)
  {
    int output = betadistribution(generator);
    if ( output < 10)
    {
      cout << "  " << output; 
    } else 
    {
     cout << " " << output; 
    }
    if ( i != num_beta-1)
      cout << ",";
  }

  cout << "},\n";
}

void gen_old_pi_vector(int num_alpha, int num_beta)
{
  cout << "{";

  // Make sure there are the approprite zeros
  for(int i = 0; i < num_alpha; i++)
  {
    cout << "  0,";
  }

  for (int i = 0; i < num_alpha; i++) 
  {
    int output = alphadistribution(generator);
    if ( output < 10)
    {
      cout << "  " << output << ","; 
    } else 
    {
     cout << " " << output << ","; 
    }
  }

  for (int i = 0; i < num_beta; i++)
  {
    int output = betadistribution(generator);
    if ( output < 10)
    {
      cout << "  " << output; 
    } else 
    {
     cout << " " << output; 
    }
    if (i != (num_beta - 1))
    {
      cout << ",";
    }
  }

  cout << "},\n";
}

void gen_new_std_vector(int num_alpha, int num_beta) 
{
  cout << "{";

  for (int i = 0; i < num_alpha; i++) 
  {
    int output = alphadistribution(generator);
    if ( output < 10)
    {
      cout << "  " << output << ","; 
    } else 
    {
     cout << " " << output << ","; 
    }
  }

  for (int i = 0; i < num_beta; i++)
  {
    int output = betadistribution(generator);
    if ( output < 10)
    {
      cout << "  " << output; 
    } else 
    {
     cout << " " << output; 
    }
    if ( i != num_beta-1)
      cout << ",";
  }

  cout << "},\n";
}

void gen_new_pi_vector(int num_alpha, int num_beta)
{
  gen_new_std_vector( num_alpha, num_beta);
} 