#include <fstream>
#include <unistd.h>
#include <vector>
#include <assert.h>

#include "FHE.h"
#include "timing.h"
#include "EncryptedArray.h"

int main(int argc, char* argv[]) 
{ 
  std::string filename = "iotest.txt";
  int num_plaintext_vectors = 0;
  int vector_dimension = 0;
  int num_ctxt = 0;
  int ctxt_to_row = 1;

  ArgMapping amap;
  amap.arg("f", filename, "Input Filename");
  amap.arg("n", num_ctxt, "Number of Ciperhtext in File");
  amap.arg("r", ctxt_to_row, "Number of Ciperhtext to a Row");
  amap.arg("v", num_plaintext_vectors, "Number of Plaintext Vectors in the data set");
  amap.arg("d", vector_dimension, "Dimension of the Plaintext Vectors");
  amap.parse(argc, argv);

  // open file for read
  fstream keyFile(filename, fstream::in);
  // Read context from file
  unsigned long m1, p1, r1;
  vector<long> gens, ords;
  readContextBase(keyFile, m1, p1, r1, gens, ords);
  FHEcontext read_context(m1, p1, r1, gens, ords);
  keyFile >> read_context;
  cout << "Finished reading in Context..." << endl;

  FHESecKey secretKey(read_context);
  const FHEPubKey& publicKey = secretKey;

  keyFile >> secretKey;
  cerr << "Finished reading in Secret Key" << endl;

  EncryptedArray ea(read_context);

  long nSlots = ea.size();
  // Sanity check
  cout << "nSlots=" << nSlots << endl;
  // Make sure there is no nonsense going on
  assert(num_plaintext_vectors == 0 || num_ctxt == 0 
    || num_plaintext_vectors * vector_dimension == num_ctxt * nSlots);


  Ctxt* encrypted_data_set[num_ctxt];

  for(int i = 0; i < num_ctxt; i++) 
  {
    encrypted_data_set[i] = new Ctxt(publicKey);
    keyFile >> *encrypted_data_set[i];
  }

  // To compute linear regression, we compute
  // \sum_{i}{x_i}
  // \sum_{i}{y_i}
  // \sum_{i}{x^2_i}
  // \sum_{i}{y^2_i}
  // \sum_{i}{y_i x_i}
  // and each of their proof's

  // For the sake of this example we let row 25 be the indicator for x's
  // and row 32 be the indicator for the y's

  // So, the method for computing each is as follows
  // sum_xi = \sum(data_set[i][7]*data_set[i][25])
  // sum_yi = \sum(data_set[i][4]*data_set[i][32])
  // sum_xxi = \sum(data_set[i][7]*data_set[i][7]*data_set[i][25])
  // sum_yyi = \sum(data_set[i][4]*data_set[i][4]*data_set[i][32])
  // sum_xyi = \sum(data_set[i][7]*data_set[i][25]*data_set[i][4]*data_set[i][32])

  cout << "Begining Ciphertext Opperations\n";

  Ctxt sum_xi(ZeroCtxtLike, *encrypted_data_set[0])    ;
  Ctxt sum_yi(ZeroCtxtLike, *encrypted_data_set[0])    ;
  Ctxt sum_xxi(ZeroCtxtLike, *encrypted_data_set[0])   ;
  Ctxt sum_yyi(ZeroCtxtLike, *encrypted_data_set[0])   ;
  Ctxt sum_xyi(ZeroCtxtLike, *encrypted_data_set[0])   ;

  // We have removed the proofs as seperate rows.  Now they are embedded 
  // Ctxt pi_sum_xi(ZeroCtxtLike, *encrypted_data_set[0]) ;
  // Ctxt pi_sum_yi(ZeroCtxtLike, *encrypted_data_set[0]) ;
  // Ctxt pi_sum_xxi(ZeroCtxtLike, *encrypted_data_set[0]);
  // Ctxt pi_sum_yyi(ZeroCtxtLike, *encrypted_data_set[0]);
  // Ctxt pi_sum_xyi(ZeroCtxtLike, *encrypted_data_set[0]);

  // Dept of each circuit
  // Sum_xi and Sum_yi
  //    Each individual ciphertext costs 2 for summation with the temp and then multiplication 
  //    Summing over the entire ciphertext is # of ciphertexts
  //    Total Depth of circuit: # of ciphertext in a row + 2

  // sum_xi
  for( int i = 0; i < ctxt_to_row; i++ )
  {
    Ctxt sum_xi_tmp(ZeroCtxtLike, *encrypted_data_set[0]);
    sum_xi_tmp += *encrypted_data_set[ctxt_to_row*7+i];
    sum_xi_tmp.multiplyBy(*encrypted_data_set[ctxt_to_row*25+i]);
    sum_xi += sum_xi_tmp;

    Ctxt sum_yi_tmp(ZeroCtxtLike, *encrypted_data_set[0]);
    sum_yi_tmp += *encrypted_data_set[ctxt_to_row*4+i];
    sum_yi_tmp.multiplyBy(*encrypted_data_set[ctxt_to_row*32+i]);
    sum_yi += sum_yi_tmp;

    Ctxt sum_xxi_tmp(ZeroCtxtLike, *encrypted_data_set[0]);
    sum_xxi_tmp += *encrypted_data_set[ctxt_to_row*7+i];
    sum_xxi_tmp.multiplyBy(*encrypted_data_set[ctxt_to_row*25+i]);
    sum_xxi_tmp.multiplyBy(*encrypted_data_set[ctxt_to_row*25+i]);
    sum_xxi += sum_xxi_tmp;

    Ctxt sum_yyi_tmp(ZeroCtxtLike, *encrypted_data_set[0]);
    sum_yyi_tmp += *encrypted_data_set[ctxt_to_row*32+i];
    sum_yyi_tmp.multiplyBy(*encrypted_data_set[ctxt_to_row*4+i]);
    sum_yyi_tmp.multiplyBy(*encrypted_data_set[ctxt_to_row*4+i]);
    sum_yyi += sum_yyi_tmp;

    Ctxt sum_xyi_tmp(ZeroCtxtLike, *encrypted_data_set[0]);
    sum_xyi_tmp += *encrypted_data_set[ctxt_to_row*32+i];
    sum_xyi_tmp.multiplyBy(*encrypted_data_set[ctxt_to_row*25+i]);
    sum_xyi_tmp.multiplyBy(*encrypted_data_set[ctxt_to_row*4+i]);
    sum_xyi_tmp.multiplyBy(*encrypted_data_set[ctxt_to_row*7+i]);
    sum_xyi += sum_xyi_tmp;
  }

  cout << "Finished Ciphertext Opperations\n";

  cout << "Starting Writing out to File\n";
  // We write the results to a file.
  fstream OutKeyFile("results.txt", fstream::out|fstream::trunc);
  assert(OutKeyFile.is_open());
  writeContextBase(OutKeyFile, read_context);
  OutKeyFile << read_context << endl;
  
  OutKeyFile << secretKey << endl;

  // Output the ciphertext to file
  OutKeyFile << sum_xi << endl;
  OutKeyFile << sum_yi << endl;
  OutKeyFile << sum_xxi << endl;
  OutKeyFile << sum_yyi << endl;
  OutKeyFile << sum_xyi << endl;

  cout << "Finished Writing out to File\n";

  cout << "Starting Decryption Process\n";

  NewPlaintextArray pp0(ea);
  NewPlaintextArray pp1(ea);
  NewPlaintextArray pp2(ea);
  NewPlaintextArray pp3(ea);
  NewPlaintextArray pp4(ea);
  ea.decrypt(sum_xi, secretKey, pp0);
  ea.decrypt(sum_yi, secretKey, pp1);
  ea.decrypt(sum_xxi, secretKey, pp2);
  ea.decrypt(sum_yyi, secretKey, pp3);
  ea.decrypt(sum_xyi, secretKey, pp4);


  cout << "Finished Decryption Process\n";

  cout << "sum_xi = " << pp0 << "\n\n\n";
  cout << "sum_yi = " << pp1 << "\n\n\n";
  cout << "sum_xxi = " << pp2 << "\n\n\n";
  cout << "sum_yyi = " << pp3 << "\n\n\n";
  cout << "sum_xyi = " << pp4 << "\n\n\n";

}