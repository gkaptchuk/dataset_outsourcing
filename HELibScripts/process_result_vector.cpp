#include <fstream>
#include <unistd.h>
#include <vector>
#include <assert.h>

#include "FHE.h"
#include "timing.h"
#include "EncryptedArray.h"

int main(int argc, char* argv[])
{
  std::string filename = "results.txt";
  int proof_index = 0;
  int num_ctxt = 0;
  int opp = 0;

  ArgMapping amap;
  amap.arg("f", filename, "Input Filename");
  amap.arg("n", num_ctxt, "Number of Ciperhtexts in File");
  amap.arg("opp", opp, "Opperation. 0=sum, 1=mult");
  amap.arg("p", proof_index, "Proof Index");

  amap.parse(argc, argv);
  cout << "Starting reading in Context\n";
  // open file for read
  fstream keyFile(filename, fstream::in);
  // Read context from file
  unsigned long m1, p1, r1;
  vector<long> gens, ords;
  readContextBase(keyFile, m1, p1, r1, gens, ords);
  FHEcontext read_context(m1, p1, r1, gens, ords);
  keyFile >> read_context;
  cout << "Finished reading in Context" << endl;
  cout << "Starting reading in Secret Key\n";

  FHESecKey secretKey(read_context);
  const FHEPubKey& publicKey = secretKey;

  keyFile >> secretKey;
  cerr << "Finished reading in Secret Key" << endl;

  EncryptedArray ea(read_context);

  long nSlots = ea.size();
  // Sanity check
  cout << "nSlots=" << nSlots << endl;

  Ctxt* encrypted_data_set[num_ctxt];

  for(int i = 0; i < num_ctxt; i++) 
  {
    encrypted_data_set[i] = new Ctxt(publicKey);
    keyFile >> *encrypted_data_set[i];
  }

  //Decrypt and sum over the entires in
  NewPlaintextArray* plaintexts[num_ctxt];
  for(int i = 0; i < num_ctxt; i++) 
  {
    plaintexts[i] = new NewPlaintextArray(ea);
    ea.decrypt(*encrypted_data_set[i], secretKey, *plaintexts[i]);
  }

  for(int i = 0; i < num_ctxt; i++) 
  {
    long proof = -1;
    vector<long> slots;
    decode(ea, slots, *plaintexts[i]);
    int r = slots[0];
    for(int j =1; j< slots.size(); j++)
    {
      if (j == proof_index) 
      {
        proof = slots[j];
      } else
      {
        if(opp == 0 )
        {
          r += slots[j];
        }
        else 
        {
          r *= slots[j];
        }
      }
    }
    cout << "R_" << i << "=" << r << endl;
    cout << "Proof_" << i << "=" << proof << endl;
  }

}