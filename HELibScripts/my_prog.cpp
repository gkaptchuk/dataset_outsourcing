#include <fstream>
#include <unistd.h>

#include "FHE.h"
#include "timing.h"
#include "EncryptedArray.h"

int main() 
{ 
  long m=20485;
  long p=23;
  long r=1;
  long L=15;
  long c=2;
  long w=64; // parameters

  ZZX G;                 // defines the plaintext space
  G = makeIrredPoly(p, 1);
  // Some code here to choose all the parameters, perhaps
  // using the fucntion FindM(...) in the FHEContext module
  FHEcontext context(m, p, r);

  cout << "Finished Context Setup\n";
  // initialize context
  buildModChain(context, L, c);

  cout << "Finished Building Mod Chain\n";

  // modify the context, adding primes to the modulus chain
  FHESecKey secretKey(context);

  // construct a secret key structure associated with the context
  const FHEPubKey& publicKey = secretKey;

  // an "upcast": FHESecKey is a subclass of FHEPubKey
  secretKey.GenSecKey(w);

  // actually generate a secret key with Hamming weight w
  addSome1DMatrices(secretKey);

  cout << "Finished Generating Secret Key\n";

 // compute key-switching matrices that we need
  EncryptedArray ea(context, G);
  // constuct an Encrypted array object ea that is
  // associated with the given context and the polynomial G
  long nslots = ea.size();
  // number of plaintext slots
  NewPlaintextArray p0(ea);
  NewPlaintextArray p1(ea);
  NewPlaintextArray p2(ea);
  NewPlaintextArray p3(ea);
  NewPlaintextArray p4(ea);

  // PlaintextArray objects associated with the given EncryptedArray ea
  encode(ea, p0, 0);
  encode(ea, p1, 10);
  encode(ea, p2, 25);
  encode(ea, p3, 0);
  encode(ea, p4, 1);

  // generate random plaintexts: slots initalized with random elements of Z[X]/(G,p^r)
  Ctxt c0(publicKey), c1(publicKey), c2(publicKey), c3(publicKey), c4(publicKey);

  // construct ciphertexts associated with the given public key
  ea.encrypt(c0, publicKey, p0);
  ea.encrypt(c1, publicKey, p1);
  ea.encrypt(c2, publicKey, p2);
  ea.encrypt(c3, publicKey, p3);
  ea.encrypt(c4, publicKey, p4);

  // encrypt each PlaintextArray
  long shamt = RandomBnd(2*(nslots/2) + 1) - (nslots/2);

  // shift-amount: random number in [-nslots/2..nslots/2]
  long rotamt = RandomBnd(2*nslots - 1) - (nslots - 1);

  // rotation-amount: random number in [-(nslots-1)..nslots-1]
  NewPlaintextArray const1(ea);
  NewPlaintextArray const2(ea);
  random(ea, const1);
  random(ea, const2);

  // two random constants
  // Perform some simple computations directly on the plaintext arrays:
  //mul(ea, p1, p0); // p1 = p1 * p0 (slot-wise modulo G)

  cout << "Begining Plaintext Opperations\n";
  add(ea, p0, p3);
  //mul(ea, p1, p3);
  //mul(ea, p2, p3);
  mul(ea, p3, p1);
  mul(ea, p4, p1);

//  add(ea, p0, const1); // p0 = p0 + const1
//  mul(ea, p2, const2); // p2 = p2 * const2
//  NewPlaintextArray tmp_p(p1); // tmp = p1
//  shift(ea, tmp_p, shamt); // shift tmp_p by shamt
//  add(ea, p2,tmp_p); // p2 = p2 + tmp_p
//  rotate(ea, p2, rotamt); // rotate p2 by rotamt
//  ::negate(ea, p1); // p1 = - p1
//  mul(ea, p3, p2); // p3 = p3 * p2
//  sub(ea,p0, p3); // p0 = p0 - p3
  cout << "Finished Plaintext Opperations\n";

  // Perform the same operations on the ciphertexts
  ZZX const1_poly, const2_poly;
  ea.encode(const1_poly, const1);
  ea.encode(const2_poly, const2);

  cout << "Begining Ciphertext Opperations\n";
  // encode const1 and const2 as plaintext polynomials
  
  c0 += c3;
  //c1.multiplyBy(c3);
  //c2.multiplyBy(c3);
  c3.multiplyBy(c1);
  c4.multiplyBy(c1);



//  c1.multiplyBy(c0); // c1 = c1 * c0
//  c0.addConstant(const1_poly); // c0 = c0 + const1
//  c2.multByConstant(const2_poly); // c2 = c2 * const2
//  Ctxt tmp(c1); // tmp = c1
//  ea.shift(tmp, shamt); // shift tmp by shamt
//  c2 += tmp; // c2 = c2 + tmp
//  ea.rotate(c2, rotamt); // rotate c2 by shamt
//  c1.negate(); // c1 = - c1
//  c3.multiplyBy(c2); // c3 = c3 * c2
//  c0 -= c3; // c0 = c0 - c3
  cout << "Finished Ciphertext Opperations\n";

  // Decrypt the ciphertexts and compare
  NewPlaintextArray pp0(ea);
  NewPlaintextArray pp1(ea);
  NewPlaintextArray pp2(ea);
  NewPlaintextArray pp3(ea);
  NewPlaintextArray pp4(ea);
  ea.decrypt(c0, secretKey, pp0);
  ea.decrypt(c1, secretKey, pp1);
  ea.decrypt(c2, secretKey, pp2);
  ea.decrypt(c3, secretKey, pp3);
  ea.decrypt(c4, secretKey, pp4);

//  cout << "Dumping the Encrypted Data\n";
//  cout << "C0.  Encryption of 5: " << c0 <<"\n";
//  cout << "C1.  Encryption of 0: " << c1 <<"\n";
//  cout << "C2.  Encryption of 0: " << c2 <<"\n";
//  cout << "C3.  Encryption of 0: " << c3 <<"\n";
//  cout << "C4.  Encryption of 0: " << c4 <<"\n";

  cout << "C3.  Should be 0: " << pp3 << "\n";
  cout << "C4.  Should be 10: " << pp4 << "\n";


  if (!equals(ea,pp0,p0)) cerr << "oops 0:  \npp0 :" << pp0 << " \np0: " << p0 << "\n";
  if (!equals(ea,pp1,p1)) cerr << "oops 1:  \npp1 :" << pp1 << " \np1: " << p1 << "\n";
  if (!equals(ea,pp2,p2)) cerr << "oops 2:  \npp2 :" << pp2 << " \np2: " << p2 << "\n";


}