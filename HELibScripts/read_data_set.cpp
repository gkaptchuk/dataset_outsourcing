#include <fstream>
#include <unistd.h>
#include <string>

#include <NTL/ZZX.h>
#include <NTL/vector.h>

#include "FHE.h"
#include "timing.h"
#include "EncryptedArray.h"

int main(int argc, char *argv[]) {

  std::string filename = "iotest.txt";
  int num_ctxt = 0;

  ArgMapping amap;
  amap.arg("f", filename, "Input Filename");
  amap.arg("n", num_ctxt, "Number of Ciperhtext in File");
  amap.parse(argc, argv);
  // open file for read
  { //Explicit scope... just because
    fstream keyFile(filename, fstream::in);
    // Read context from file
    unsigned long m1, p1, r1;
    vector<long> gens, ords;
    readContextBase(keyFile, m1, p1, r1, gens, ords);
    FHEcontext read_context(m1, p1, r1, gens, ords);
    keyFile >> read_context;
    cout << "Finished reading in Context..." << endl;

    FHESecKey secretKey(read_context);
    const FHEPubKey& publicKey = secretKey;

    keyFile >> secretKey;
    cerr << "Finished reading in Secret Key" << endl;

    EncryptedArray ea(read_context);

    long nSlots = ea.size();
    // Sanity check
    cout << "nSlots=" << nSlots << endl;

    Ctxt* encrypted_data_set[num_ctxt];

    for(int i = 0; i < num_ctxt; i++) 
    {
      encrypted_data_set[i] = new Ctxt(publicKey);
      keyFile >> *encrypted_data_set[i];
    }

    cout << "Finished reading in " << num_ctxt << " Ciphertexts..." << endl;
  }
}